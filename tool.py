_pageCache={}
def getUrl(url):
    global _pageCache
    #print("getting "+url)
    import requests
    if not url in _pageCache:
        import time
        # prevent overload
        time.sleep(1)
        sucess = False
        while not sucess:
            r = requests.get(url)
            if r.status_code == 200:
                html = r.text
                sucess = True
            else:
                import time
                time.sleep(2)
        if sucess:
            _pageCache[url] = html
    else:
        sucess = True
        html = _pageCache[url]

    obtained = []
    return html

def parseHtml(url):
    html = getUrl(url)

    from lxml import etree
    data = etree.fromstring(html, etree.HTMLParser())

    return data

def parseJson(url):
    data = getUrl(url)

    import json

    return json.loads(data)

def escape(text):
    # replace / by &sla et & by &&&
    retour = ""
    for loop in text:
        if loop == "/":
            retour += "&sla"
        elif loop == "\\":
            retour += "&sb"
        elif loop == "&":
            retour += "&&&"
        else:
            retour += loop
    return retour

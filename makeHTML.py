import os
import json

def tableToStr(t):
    a = ""
    for loop in t:
        a += loop + "."
    return a[:-1]

def comicHTML(location):
    data = []
    for loop in os.listdir(location):
        print('-----')
        if loop.split(".")[-1] == "json":
            continue
        if loop.split(".")[-1] not in ["png","jpg","svg"]:
            assert loop.split(".")[-1] in ["html","directory"]
            continue
        fileL = location + "/" + loop
        temp = open(tableToStr(fileL.split(".")[:-1])+".json")


        js = json.loads(temp.read())
        js["page"] = int(loop.split("-")[0][:-1])
        js["filename"] = tableToStr(fileL.split(".")[:-1]).split("/")[-1]
        js["extension"] = fileL.split(".")[-1]
        data.append(js)

    for loop in range(len(data)):
        adata = data[loop]
        if loop+1 < len(data):
            nextdata = data[loop+1]
        else:
            nextdata = None

        if loop != 0:
            previousdata = data[loop-1]
        else:
            previousdata = None

        # generate code. actually bad.
        html = "<body>\n"
        html += "<h1>"+adata["name"]+"</h1>\n"
        if nextdata != None:
            html += "<a href='"+nextdata["filename"]+".html'>\n"
        html += "<img src='"+adata["filename"]+"."+adata["extension"]+"' />\n"
        if nextdata != None:
            html += "</a>\n"
        html += "</body>"

        tempF = open(location+"/"+adata["filename"]+".html","w")
        tempF.write(html)
        tempF.close()


        print(html)



folder = "backup/comics/"

for loop in os.listdir(folder):
    comicHTML(folder+loop)

from tool import parseHtml
from lxml import etree
import wget
import os
import json

_version = 1
def getComic(comicurl,location,baseUrl="http://ssp-comics.com"):
    html = parseHtml(comicurl)
    pages = []
    for table in html.iter("table"):
        if table.attrib.get("class") == "pagelist":
            body = table.find("tbody")
            for page in body:
                apage = {}
                element = page.findall("td")
                if len(element) == 3:
                    apage["url"] = baseUrl+element[0].find("a").attrib.get("href")
                    apage["name"] = element[1].text
                    apage["date"] = element[2].text
                    pages.append(apage)

    #for infocontainer in html.iter("div"):
    #    if infocontainer.attrib.get("class") == "infoContainer":
    #
    # TODO:

    comicLocation = location + "/comics/" + comicurl.split("/")[-1]
    os.makedirs(comicLocation,exist_ok=True)
    for loop in pages:
        getPage(loop,comicLocation)

    # TODO: write metadata

def getPage(page,location,baseUrl="http://ssp-comics.com"):
    # TODO: test if metadata exist and are correct
    pageNB = page["url"].split("=")[1]

    loc = location+"/"+cleanText("0"*(4-len(str(pageNB)))+str(pageNB)+ " - " + page["name"])

    redownloadPage = False
    redownloadImage = False
    updateData = False

    if os.path.isfile(loc+".json"):
        tempf = open(loc+".json")
        data = json.loads(tempf.read())
        tempf.close()

        if data.get("ssp-comid-dl version") == "0.1.1":
            data["dlversion"] = 1
            data.pop("ssp-comid-dl version")
            updateData = True


        if updateData:
            metadataF = open(loc+".json","w")
            metadataF.write(json.dumps(data))
            metadataF.close()
            print(data)
    else:
        redownloadImage = True
        redownloadPage = True

    if redownloadImage or redownloadPage:
        print("downloading "+page["name"])
        html = parseHtml(page["url"])
        for ImageComicContainer in html.iter("div"):
            if ImageComicContainer.attrib.get("id") == "ImageComicContainer":
                a = ImageComicContainer.find("a")
                img = a.find("img")
                imgurl = baseUrl + img.attrib["src"]

        jsonData = {
            "ssp-comid-dl version" : _version,
            "name" : page["name"],
            "last modified" : page["date"],
            "url" : page["url"]
        }

        # download the pic
        if redownloadImage:
            wget.download(imgurl,loc+"."+imgurl.split(".")[-1])
        # write the metadata
        metadataF = open(loc+".json","w")
        metadataF.write(json.dumps(jsonData))
        metadataF.close()
    else:
        print(page["name"] + " already downloaded.")

def getAllComic(url,location,baseUrl="http://ssp-comics.com"):
    html = parseHtml(url)

    for comicElement in html.iter("div"):
        if comicElement.attrib.get("class") == "comicElement":
            infoContainer = comicElement.findall("div")[1]
            for link in infoContainer.iter("p"):
                if link.attrib.get("class") == "center":
                    a = link.findall("a")
                    urlComic = baseUrl+a[0].attrib.get("href")
            getComic(urlComic,location,baseUrl)

def cleanText(texte):
    rendu = ""
    for loop in texte:
        if loop == "/":
            rendu += "&s"
        elif loop == "&":
            rendu += "&&"
        else:
            rendu += loop
    return rendu

location = "./backup"
getAllComic("http://ssp-comics.com/series",location)
